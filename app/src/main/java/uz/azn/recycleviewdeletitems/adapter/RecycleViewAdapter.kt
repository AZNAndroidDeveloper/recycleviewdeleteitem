package uz.azn.recycleviewdeletitems.adapter

import android.os.CountDownTimer
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.azn.recycleviewdeletitems.MainActivity
import uz.azn.recycleviewdeletitems.R
import uz.azn.recycleviewdeletitems.introFragment.IntroFragment
import uz.azn.recycleviewdeletitems.model.App

class RecycleViewAdapter(list: MutableList<App>, introFragment: IntroFragment) :
    RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>() {
    val introFragment1: IntroFragment
    var itemlist: MutableList<App> = ArrayList()
    var selection2: MutableList<App> = ArrayList()

    init {
        this.itemlist = list
        this.introFragment1 = introFragment
    }

    inner class MyViewHolder(itemView: View, introFragment: IntroFragment) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val imageView = itemView.findViewById<ImageView>(R.id.image_view)
        val textView = itemView.findViewById<TextView>(R.id.text_tv)
        val checkBox = itemView.findViewById<CheckBox>(R.id.chk_box)
        var view: View = itemView

        init {
            view.setOnLongClickListener(introFragment1)
            checkBox.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            introFragment1.selectItem(v, adapterPosition)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.items_layout, null, false)
        return MyViewHolder(view, introFragment1)
    }

    override fun getItemCount(): Int = itemlist.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imageView.setImageResource(itemlist[position].image)
        holder.textView.text = itemlist[position].title
        if (!introFragment1.icContexualModeEnabled) {
            holder.checkBox.visibility = View.INVISIBLE
        } else {
            holder.checkBox.visibility = View.VISIBLE
            holder.checkBox.isChecked = false
        }
    }

    fun removeItem(selectionList: MutableList<App>, isHave: Boolean) {
        if (isHave) {
            for (i in selectionList.indices) {
                itemlist.remove(selectionList[i])
                selection2.add(selectionList[i])
            }
            Handler().postDelayed(Runnable {
                notifyDataSetChanged()
                selection2.clear()
            }, 6000)

        } else {
            for (i in selection2.indices) {
                itemlist.add(selection2[i])
                notifyDataSetChanged()
            }
            selection2.clear()
        }

    }
}

