package uz.azn.recycleviewdeletitems.introFragment

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.*
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import uz.azn.recycleviewdeletitems.MainActivity
import uz.azn.recycleviewdeletitems.R
import uz.azn.recycleviewdeletitems.adapter.RecycleViewAdapter
import uz.azn.recycleviewdeletitems.model.App
import java.text.FieldPosition

class IntroFragment(var mContext: Context?) : Fragment(), View.OnLongClickListener {
    init {
        mContext = context
    }

    lateinit var appList: MutableList<App>
    lateinit var recycleViewAdapter: RecycleViewAdapter
    var icContexualModeEnabled = false
    var counter = 0
    private val images: MutableList<Int> = arrayListOf(
        R.drawable.inesta,
        R.drawable.mbappe,
        R.drawable.messi,
        R.drawable.odil,
        R.drawable.pogba,
        R.drawable.ralando,
        R.drawable.zidane
    )
    private lateinit var titles: Array<String>
    private lateinit var itemList: MutableList<App>
    private lateinit var selectionList: MutableList<App>
    private val defaultText = "0 item Select"
    lateinit var toolbar: Toolbar
    lateinit var itemCounter: TextView
    lateinit var recycle: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_intro, container, false)
        setHasOptionsMenu(true)
        toolbar = view.findViewById(R.id.toolbar_view)
        itemCounter = view.findViewById(R.id.itemCounter)
        recycle = view.findViewById(R.id.recycle_view)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        itemCounter.text = getString(R.string.app_name)
        titles = resources.getStringArray(R.array.image_title)
        itemList = ArrayList()
        appList = arrayListOf()
        selectionList = ArrayList()
        for (i in titles.indices) {
            val app = App(images[i], titles[i])
            itemList.add(app)
        }
        recycleViewAdapter = RecycleViewAdapter(itemList, this)
        recycle.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        recycle.adapter = recycleViewAdapter

        return view
    }

    fun selectItem(view: View?, adapterPosition: Int) {
        if ((view as CheckBox).isChecked) {
            selectionList.add(itemList[adapterPosition])

            counter++
        } else {
            selectionList.remove(itemList[adapterPosition])
            counter--
        }
        updateCounterUi()

    }

    private fun updateCounterUi() {
        itemCounter.text = "$counter  item select"

    }

    override fun onLongClick(v: View?): Boolean {
        icContexualModeEnabled = true
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.contexual_menu)
        activity?.actionBar?.title = defaultText
        itemCounter.text = defaultText
        toolbar.setBackgroundColor(Color.parseColor("#FF03DAC5"))
        recycleViewAdapter.notifyDataSetChanged()
        // orqaga qaytish knopka

        setDisplay(true)
        return true
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.normal_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.delete) {
            snackBar(view)
            recycleViewAdapter.removeItem(selectionList,true)
            remoteContexualMenu(true)
        } else if (item.itemId == android.R.id.home) {
            remoteContexualMenu(true)

            recycleViewAdapter.notifyDataSetChanged()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun remoteContexualMenu(a:Boolean) {
        icContexualModeEnabled = false
        itemCounter.text = getString(R.string.app_name)
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.normal_menu)
        counter = 0
        if (a){
            selectionList.clear()
            recycleViewAdapter.notifyDataSetChanged()
            toolbar.setBackgroundColor(Color.parseColor("#FF3700B3"))
            setDisplay(false)
        }
        else{
            recycleViewAdapter.notifyDataSetChanged()
            toolbar.setBackgroundColor(Color.parseColor("#FF3700B3"))
            setDisplay(false)

        }
    }

    private fun setDisplay(isHave: Boolean) {
        if ((activity as MainActivity).supportActionBar != null) {
            val actionBar = (activity as MainActivity).supportActionBar
            actionBar?.setDisplayHomeAsUpEnabled(isHave)
            actionBar?.setDisplayShowHomeEnabled(true)

        }
    }
    private fun snackBar(view: View?){
        val snack = Snackbar.make(view!!,"This image delete",6000)

        snack.setAction("Dissmis",View.OnClickListener {
                recycleViewAdapter.removeItem(selectionList,false)
                remoteContexualMenu(false)

        }).show()
    }



}