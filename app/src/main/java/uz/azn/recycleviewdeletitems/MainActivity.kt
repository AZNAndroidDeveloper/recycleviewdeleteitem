package uz.azn.recycleviewdeletitems

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.recycleviewdeletitems.databinding.ActivityMainBinding
import uz.azn.recycleviewdeletitems.introFragment.IntroFragment

class MainActivity : AppCompatActivity() {
    val binding by lazy{ActivityMainBinding.inflate(layoutInflater)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.frame_layout,IntroFragment(this)).commit()
    }
}